﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace OOP
{
    internal class db_view : db_property
    {
        public DataTable data_table(string sql)
        {
            DataTable dataTable = new DataTable();
            using (SqlConnection con = new SqlConnection(db_string_con_sql))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, con))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            dataTable.Load(reader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return dataTable;
        }

        public void view_table(DataGridView dgr, string sql)
        {
            dgr.DataSource = data_table(sql);
        }
        public void view_table(DataGridView dgr, string sql, int w_code)
        {
            //dgr.DataSource = data_table(sql + $" WHERE ID = " + w_code.ToString());
        }

        public void view_table(DataGridView dgr, string sql, string w_code)
        {
            //dgr.DataSource = data_table(sql + $" WHERE student_code = '{w_code}'");
        }


    }
}
