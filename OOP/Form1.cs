﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace OOP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            chk_by_student_code.CheckedChanged += new EventHandler(checkBox3_CheckedChanged);
            chk_by_id.CheckedChanged += new EventHandler(checkBox3_CheckedChanged);


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            db_view view = new db_view();
            view.view_table(dgr, view.student_all);
            view.view_table(dgr1, view.class_all);
        }

        private void dgr_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            db_view view = new db_view();
            using (SqlConnection con = new SqlConnection(view.db_string_con_sql))
            {
                try
                {
                    con.Open();
                    string DATA = "INSERT INTO tbl_class (class_name,floor,des)VALUES (@class_name,@floor,@des)";
                    using (SqlCommand cmd = new SqlCommand(DATA, con))
                        try
                        {
                            cmd.Parameters.AddWithValue("@class_name", txt_name.Text);
                            cmd.Parameters.AddWithValue("@floor", txt_floor.Text);
                            cmd.Parameters.AddWithValue("@des", txt_des.Text);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Save success");
                            view.view_table(dgr1, view.class_all);
                            txt_des.Clear();
                            txt_floor.Clear();
                            txt_name.Clear();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            db_view view = new db_view();
            view.view_table(dgr, view.student_all);

            if (chk_by_student_code.Checked && !string.IsNullOrEmpty(txt_search.Text))
            {
                view.view_table(dgr, view.student_all + $" WHERE student_code = '{txt_search.Text}'");
            }
            if (chk_by_id.Checked && !string.IsNullOrEmpty(txt_search.Text))
            {
                view.view_table(dgr, view.student_all + $" WHERE ID = '{txt_search.Text}'");
            }
            if (chk_by_name.Checked && !string.IsNullOrEmpty(txt_search.Text))
            {
                view.view_table(dgr, view.student_all + $" WHERE first_name = '{txt_search.Text}'");
            }
            txt_search.Clear();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void checkBox3_CheckedChanged_1(object sender, EventArgs e)
        {

        }
    }
}
