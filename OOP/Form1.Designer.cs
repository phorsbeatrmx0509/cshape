﻿namespace OOP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgr = new System.Windows.Forms.DataGridView();
            this.dgr1 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_floor = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_des = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_search = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.chk_by_all = new System.Windows.Forms.CheckBox();
            this.chk_by_student_code = new System.Windows.Forms.CheckBox();
            this.chk_by_id = new System.Windows.Forms.CheckBox();
            this.chk_by_name = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgr1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgr
            // 
            this.dgr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgr.Location = new System.Drawing.Point(100, 258);
            this.dgr.Name = "dgr";
            this.dgr.Size = new System.Drawing.Size(862, 180);
            this.dgr.TabIndex = 0;
            this.dgr.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgr_CellContentClick);
            // 
            // dgr1
            // 
            this.dgr1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgr1.Location = new System.Drawing.Point(342, 17);
            this.dgr1.Name = "dgr1";
            this.dgr1.Size = new System.Drawing.Size(620, 147);
            this.dgr1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name : ";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(100, 17);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(206, 20);
            this.txt_name.TabIndex = 3;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(231, 170);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(75, 23);
            this.btn_save.TabIndex = 4;
            this.btn_save.Text = "Save";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // txt_floor
            // 
            this.txt_floor.Location = new System.Drawing.Point(100, 43);
            this.txt_floor.Name = "txt_floor";
            this.txt_floor.Size = new System.Drawing.Size(206, 20);
            this.txt_floor.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Floor : ";
            // 
            // txt_des
            // 
            this.txt_des.Location = new System.Drawing.Point(100, 69);
            this.txt_des.Multiline = true;
            this.txt_des.Name = "txt_des";
            this.txt_des.Size = new System.Drawing.Size(206, 95);
            this.txt_des.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Description : ";
            // 
            // txt_search
            // 
            this.txt_search.Location = new System.Drawing.Point(727, 229);
            this.txt_search.Multiline = true;
            this.txt_search.Name = "txt_search";
            this.txt_search.Size = new System.Drawing.Size(154, 23);
            this.txt_search.TabIndex = 9;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(887, 229);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(75, 23);
            this.btn_search.TabIndex = 10;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // chk_by_all
            // 
            this.chk_by_all.AutoSize = true;
            this.chk_by_all.Location = new System.Drawing.Point(301, 229);
            this.chk_by_all.Name = "chk_by_all";
            this.chk_by_all.Size = new System.Drawing.Size(70, 17);
            this.chk_by_all.TabIndex = 11;
            this.chk_by_all.Text = "Select All";
            this.chk_by_all.UseVisualStyleBackColor = true;
            // 
            // chk_by_student_code
            // 
            this.chk_by_student_code.AutoSize = true;
            this.chk_by_student_code.Location = new System.Drawing.Point(582, 231);
            this.chk_by_student_code.Name = "chk_by_student_code";
            this.chk_by_student_code.Size = new System.Drawing.Size(139, 17);
            this.chk_by_student_code.TabIndex = 12;
            this.chk_by_student_code.Text = "Select By StudentCode ";
            this.chk_by_student_code.UseVisualStyleBackColor = true;
            this.chk_by_student_code.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // chk_by_id
            // 
            this.chk_by_id.AutoSize = true;
            this.chk_by_id.Location = new System.Drawing.Point(488, 231);
            this.chk_by_id.Name = "chk_by_id";
            this.chk_by_id.Size = new System.Drawing.Size(88, 17);
            this.chk_by_id.TabIndex = 13;
            this.chk_by_id.Text = "Select By ID ";
            this.chk_by_id.UseVisualStyleBackColor = true;
            this.chk_by_id.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // chk_by_name
            // 
            this.chk_by_name.AutoSize = true;
            this.chk_by_name.Location = new System.Drawing.Point(377, 230);
            this.chk_by_name.Name = "chk_by_name";
            this.chk_by_name.Size = new System.Drawing.Size(105, 17);
            this.chk_by_name.TabIndex = 14;
            this.chk_by_name.Text = "Select By Name ";
            this.chk_by_name.UseVisualStyleBackColor = true;
            this.chk_by_name.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(974, 450);
            this.Controls.Add(this.chk_by_name);
            this.Controls.Add(this.chk_by_id);
            this.Controls.Add(this.chk_by_student_code);
            this.Controls.Add(this.chk_by_all);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.txt_search);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_des);
            this.Controls.Add(this.txt_floor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgr1);
            this.Controls.Add(this.dgr);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgr1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgr;
        private System.Windows.Forms.DataGridView dgr1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox txt_floor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_des;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_search;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.CheckBox chk_by_all;
        private System.Windows.Forms.CheckBox chk_by_student_code;
        private System.Windows.Forms.CheckBox chk_by_id;
        private System.Windows.Forms.CheckBox chk_by_name;
    }
}

